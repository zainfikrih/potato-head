package men.ngopi.zain.potatohead;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

public class MainActivity extends AppCompatActivity {

    //Floating Action
    private FloatingActionMenu menu, menu2;
    private FloatingActionButton b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10;

    private ImageView body, hat, eyebrows, nose, mustache, arms, eyes, glasses, mouth, ears, shoes;
    private Button show, hide;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hat = (ImageView) findViewById(R.id.hat);
        eyebrows = (ImageView) findViewById(R.id.eyebrows);
        nose = (ImageView) findViewById(R.id.nose);
        mustache = (ImageView) findViewById(R.id.mustache);
        arms = (ImageView) findViewById(R.id.arms);
        eyes = (ImageView) findViewById(R.id.eyes);
        glasses = (ImageView) findViewById(R.id.glasses);
        mouth = (ImageView) findViewById(R.id.mouth);
        ears = (ImageView) findViewById(R.id.ears);
        shoes = (ImageView) findViewById(R.id.shoes);
        body = (ImageView) findViewById(R.id.body);

        menu = (FloatingActionMenu) findViewById(R.id.menu);
        menu2 = (FloatingActionMenu) findViewById(R.id.menu2);
        menu.setClosedOnTouchOutside(true); //click outside
        menu2.setClosedOnTouchOutside(true); //click outside

        b0 = (FloatingActionButton) findViewById(R.id.menu_item0);
        b1 = (FloatingActionButton) findViewById(R.id.menu_item);
        b2 = (FloatingActionButton) findViewById(R.id.menu_item2);
        b3 = (FloatingActionButton) findViewById(R.id.menu_item3);
        b4 = (FloatingActionButton) findViewById(R.id.menu_item4);
        b5 = (FloatingActionButton) findViewById(R.id.menu_item5);
        b6 = (FloatingActionButton) findViewById(R.id.menu_item6);
        b7 = (FloatingActionButton) findViewById(R.id.menu_item7);
        b8 = (FloatingActionButton) findViewById(R.id.menu_item8);
        b9 = (FloatingActionButton) findViewById(R.id.menu_item9);
        b10 = (FloatingActionButton) findViewById(R.id.menu_item10);
//        show = (Button) findViewById(R.id.show);
//        hide = (Button) findViewById(R.id.hide);

//        show.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hat.setVisibility(View.VISIBLE);
//                eyebrows.setVisibility(View.VISIBLE);
//                nose.setVisibility(View.VISIBLE);
//                mustache.setVisibility(View.VISIBLE);
//                arms.setVisibility(View.VISIBLE);
//                eyes.setVisibility(View.VISIBLE);
//                glasses.setVisibility(View.VISIBLE);
//                mouth.setVisibility(View.VISIBLE);
//                ears.setVisibility(View.VISIBLE);
//                shoes.setVisibility(View.VISIBLE);
//            }
//        });
//
//        hide.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hat.setVisibility(View.INVISIBLE);
//                eyebrows.setVisibility(View.INVISIBLE);
//                nose.setVisibility(View.INVISIBLE);
//                mustache.setVisibility(View.INVISIBLE);
//                arms.setVisibility(View.INVISIBLE);
//                eyes.setVisibility(View.INVISIBLE);
//                glasses.setVisibility(View.INVISIBLE);
//                mouth.setVisibility(View.INVISIBLE);
//                ears.setVisibility(View.INVISIBLE);
//                shoes.setVisibility(View.INVISIBLE);
//            }
//        });

        //Set Button Listener
        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 0);
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 2);
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 3);
            }
        });

        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 4);
            }
        });

        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 5);
            }
        });

        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 6);
            }
        });

        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 7);
            }
        });

        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 8);
            }
        });

        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 9);
            }
        });

        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 10);
            }
        });

    }

    //To Set Image
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {

            case 0:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    body.setImageURI(selectedImage);
                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    hat.setImageURI(selectedImage);
                }

                break;
            case 2:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    eyes.setImageURI(selectedImage);
                }

                break;
            case 3:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    eyebrows.setImageURI(selectedImage);
                }

                break;
            case 4:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    glasses.setImageURI(selectedImage);
                }

                break;
            case 5:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    nose.setImageURI(selectedImage);
                }

                break;
            case 6:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    mouth.setImageURI(selectedImage);
                }

                break;
            case 7:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    mustache.setImageURI(selectedImage);
                }

                break;
            case 8:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    ears.setImageURI(selectedImage);
                }

                break;
            case 9:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    arms.setImageURI(selectedImage);
                }

                break;
            case 10:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    shoes.setImageURI(selectedImage);
                }
                break;
        }
    }

//    public void toggleClick(View view) {
//        if(view.getVisibility() == View.VISIBLE){
//            view.setVisibility(View.INVISIBLE);
//        } else {
//            view.setVisibility(View.VISIBLE);
//        }
//    }
}
